<?php
use yii\helpers\Html;
use yii\widgets\Pjax;
?>

<?php Pjax::begin(['timeout' => 5000 ]); ?>

<div class="content">
        <div class="field">
            <?php foreach($circles as $circle):?>
                <div class="circle" style="
                        width:<?=$circle->radius*2?>px;
                        height:<?=$circle->radius*2?>px;
                        background:<?=$circle->color?>;
                        left: <?=$circle->left?>px;
                        top: <?=$circle->top?>px;
                        "></div>
            <?php endforeach;?>
        </div>
        <div class="field-bt">
            <?=Html::a( 'Новая точка', ['new'], ['class' => 'new-dot'] )?>
            <?=Html::a( 'Проверить', ['check'], ['class' => 'dot-check','id'=>'check'] )?>
        </div>
</div>

<!-- Modal -->
<div id="check-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <button type="button" class="btn btn-danger" id="clear-circle">Очистить</button>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>

    </div>
</div>
<?php Pjax::end(); ?>