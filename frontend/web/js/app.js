$(document).ready(function () {
    $('body').delegate('#check','click',function (e) {
        e.preventDefault();
        $.post('/circle/check',{},function (data) {
            $('#check-modal .modal-title').text('Пересечения у ' + data + ' кругов');
            if(data == 0) {
                $('#clear-circle').prop('disabled','true');
            } else {
                $('#clear-circle').removeAttr('disabled');
            }
            $('#check-modal').modal('show');
        })
    })

    $('body').delegate('#clear-circle','click',function (e) {
        $(this).prop('disabled','true');
        $.post('/circle/clear',{},function (data) {
            $('#clear-circle').removeAttr('disabled');
            if(data != 'OK') {
                alert('Попробуйте еще раз!');
            } else {
                $('#check-modal').modal('hide');
                $('.field').html('');
            }
        })
    })
})