<?php

use yii\db\Migration;

/**
 * Handles adding ri to table `user`.
 */
class m170324_171700_add_ri_column_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user', 'role_id', $this->integer());
        $this->addColumn('user', 'img', $this->string(255));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user', 'role_id');
        $this->dropColumn('user', 'img');
    }
}
