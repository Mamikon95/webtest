<?php
namespace frontend\controllers;

use Yii;
use common\models\Circle;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use common\models\LoginForm;

/**
 * Site controller
 */
class CircleController extends Controller
{

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        if(Yii::$app->user->isGuest) {
            return $this->redirect('/site/login');
        } else {
            $user_id = yii::$app->user->id;
            $circles = Circle::find()->where(['user_id'=>$user_id])->all();
            return $this->render('index',[
                'circles' => $circles
            ]);
        }
    }

    public function actionNew() {
        if(yii::$app->request->isPjax && !Yii::$app->user->isGuest) {
            $circle = new Circle();
            $circle->user_id = yii::$app->user->id;
            $circle->radius = rand (10, 40);
            $circle->top = rand (0, (500 - $circle->radius*2));
            $circle->left = rand (0, (500 - $circle->radius*2));
            $circle->color = $this->getColor();
            $circle->save();
            return $this->actionIndex();
        } else {
            return $this->redirect('/circle');
        }
    }

    public function actionCheck() {
        if(yii::$app->request->isAjax && !Yii::$app->user->isGuest) {
            $user_id = yii::$app->user->id;
            $circles = Circle::find()->where([]);
            $user_id = yii::$app->user->id;
            $countC = Circle::getCheckSql($user_id);
            echo $countC;
        } else {
            return $this->redirect('/circle');
        }
    }

    public function actionClear() {
        if(yii::$app->request->isAjax && !Yii::$app->user->isGuest) {
            $user_id = yii::$app->user->id;
            Circle::deleteAll(['user_id'=>$user_id]);
            echo 'OK';
        } else {
            return $this->redirect('/circle');
        }
    }

    private function getColor() {
        $rand = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f');
        $color = '#'.$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)];
        return $color;
    }

}
