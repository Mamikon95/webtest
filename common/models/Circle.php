<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "circle".
 *
 * @property integer $id
 * @property integer $left
 * @property integer $top
 * @property integer $radius
 * @property string $color
 * @property integer $user_id
 *
 * @property User $user
 */
class Circle extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'circle';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['left', 'top', 'radius', 'user_id'], 'integer'],
            [['color'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'left' => 'Left',
            'top' => 'Top',
            'radius' => 'Radius',
            'color' => 'Color',
            'user_id' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public static function getCheckSql($user_id) {
        $sql = 'SELECT COUNT(c1.`id`) as `cnt` FROM `circle` AS c1 
                INNER JOIN `circle` AS c2 ON c1.`user_id` = c2.`user_id` 
                WHERE SQRT(POW(((c1.`top` + c1.`radius`) - (c2.`top` + c2.`radius`)),2) + POW(((c1.`left` + c1.`radius`) - (c2.`left` + c2.`radius`)),2)) <= (c1.`radius` + c2.`radius`) AND c1.`id` <> c2.`id`';
        $res = yii::$app->db->createCommand($sql)->queryOne();
        return $res['cnt'];
    }
}
