<?php

use yii\db\Migration;

/**
 * Handles the creation of table `circle`.
 * Has foreign keys to the tables:
 *
 * - `user`
 */
class m170425_124139_create_circle_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('circle', [
            'id' => $this->primaryKey(),
            'left' => $this->integer(),
            'top' => $this->integer(),
            'radius' => $this->integer(),
            'color' => $this->string(255),
            'user_id' => $this->integer(),
        ],'DEFAULT CHARSET=utf8');

        // creates index for column `user_id`
        $this->createIndex(
            'idx-circle-user_id',
            'circle',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-circle-user_id',
            'circle',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-circle-user_id',
            'circle'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx-circle-user_id',
            'circle'
        );

        $this->dropTable('circle');
    }
}
